*********************************************************************************
*      Readme for PAVOREAL (Parallel Volume Rendering Algorithm with CUDA)      *
*********************************************************************************

Harvard University
Computer Science 205
Fall 2013

=== Team Members ===

  Fernando Beccera
  Xinyi Guo
  Philip Mocz


=== Description ===

The project is contained in the folder Pavoreal/

To run a basic visualization, set up a configuration file (e.g. in the style of
one of the provided config files in the subfolder config_files/) and type 

'python pavoreal.py [config file]' 

in the terminal or use the run.qsub submit script to submit the MPI/GPU job on 
the headnode.

Two example configuration files (Pavoreal/config_files/config_ExampleShock.ini 
and Pavoreal/config_files/config_ExampleTurb.ini) and corresponding data sets 
(data/ExampleTurb and data/ExampleShock) are included in the repository to test 
the code:

  * ExampleTurb illustrates how to use Pavoreal to volume render arbitrarily 
    gridded data with a Gaussian transfer function.

  * ExampleShock illustrates how to use Pavoreal to volume render gridded data 
    with linear transfer function and periodic camera path

The software volume renders the data set specified by the user in the 
configuration file. The user has to option to define their own Gaussian or 
linear transfer functions, camera path, and output image size. For a detailed
description of the configuration file parameters, see:

https://www.cfa.harvard.edu/~fbecerra/pavoreal/download.html

Use pavoreal_combined_kernels.py instead of pavoreal.py for a faster, better, 
less memory-intensive, optimized implementation.

To run the code in serial, use pavoreal_serial.py instead of pavoreal.py. 
Warning, it will be very slow.


=== Requirements ===

To run Pavoreal, the following Python libraries are required:

* numpy
* matplotlib
* scipy
* mpi4py
* pyCUDA


=== File Listing ===

  readme.txt                      this file

  Pavoreal/
    pavoreal.py                   main driver file for Approach 1
    pavoreal_combined_kernels.py  main driver file for Approach 2 (faster)
    pavoreal_serial.py            driver file for serial code
    config_files/                 contains configuration files 
    pavoreal/                     volume rendering package

  data/                           some sample data to run examples
    ExampleTurb/                  2 of unstructured grid example data         
     turb15_050.hdf5
     turb15_051.hdf5
    ExampleShock/                 2 frames of gridded example data         
     flds.040
     flds.041
     
  output/                         some sample volume renderings with Pavoreal
    ExampleTurb/                  uses data: data/ExampleTurb/  
                                  and config: ExampleTurb.ini          

    ExampleShock/                 uses data: data/ExampleShock/ 
                                  and config: ExampleShock.ini         
