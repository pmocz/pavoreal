from include import *

interp_texture_kernel_source = \
"""
texture<float, 3, cudaReadModeElementType> data_tex;
   __global__ void interp_texture_kernel(float *dest, float* sample_x, float* sample_y, float* sample_z, int Nsample, int Niterations, int Nthreads)
   {
     for (int iteration = 0; iteration < Niterations; ++iteration)
      {

       int gid = blockIdx.x * blockDim.x * blockDim.y * blockDim.z + threadIdx.z *  blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x + iteration * Nthreads;
  
       if (gid < Nsample)
             dest[gid] = tex3D(data_tex, sample_x[gid], sample_y[gid], sample_z[gid]);
      }
   }
"""

# based on Ruijters+09 "Efficient GPU-Based Texture Interpolation using Uniform B-Splines" http://www.mate.tue.nl/mate/pdfs/10318.pdf
cubic_spline_interp_texture_kernel_source = \
"""
inline  __device__ float3 operator+(const float3 &a, const float3 &b) {
  return make_float3(a.x+b.x, a.y+b.y, a.z+b.z);
}

inline  __device__ float3 operator-(const float3 &a, const float3 &b) {
  return make_float3(a.x-b.x, a.y-b.y, a.z-b.z);
}

inline  __device__ float3 operator*(const float3 &a, const float3 &b) {
  return make_float3(a.x*b.x, a.y*b.y, a.z*b.z);
}

inline  __device__ float3 operator/(const float3 &a, const float3 &b) {
  return make_float3(a.x/b.x, a.y/b.y, a.z/b.z);
}

__device__ float lerp(float a, float b, float w) {
  return a + w*(b-a);
}


texture<float, 3, cudaReadModeElementType> data_tex;
   __global__ void cubic_spline_interp_texture_kernel(float *dest, float* sample_x, float* sample_y, float* sample_z, int Nsample, int Niterations, int Nthreads)
   {
     for (int iteration = 0; iteration < Niterations; ++iteration)
      {

       int gid = blockIdx.x * blockDim.x * blockDim.y * blockDim.z + threadIdx.z *  blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x + iteration * Nthreads;

       if (gid < Nsample) {
        float x = sample_x[gid];
        float y = sample_y[gid];
        float z = sample_z[gid];
        // shift coordinates by -0.5
        float3 coord_grid = make_float3(x-0.5, y-0.5, z-0.5);
        float3 index = make_float3(floor(coord_grid.x), floor(coord_grid.y), floor(coord_grid.z));
        float3 fraction = coord_grid - index;
        float3 one_frac = make_float3(1.0-fraction.x, 1.0-fraction.y, 1.0-fraction.z);
        float3 one_frac2 = one_frac * one_frac;
        float3 fraction2 = fraction * fraction;
        float3 w0 = make_float3(1.0/6.0*one_frac2.x*one_frac.x, 1.0/6.0*one_frac2.y*one_frac.y, 1.0/6.0*one_frac2.z*one_frac.z);
        float3 w1 = make_float3(2.0/3.0-0.5*fraction2.x*(2.0-fraction.x), 2.0/3.0-0.5*fraction2.y*(2.0-fraction.y), 2.0/3.0-0.5*fraction2.z*(2.0-fraction.z));
        float3 w2 = make_float3(2.0/3.0-0.5*one_frac2.x*(2.0-one_frac.x), 2.0/3.0-0.5*one_frac2.y*(2.0-one_frac.y),2.0/3.0-0.5*one_frac2.z*(2.0-one_frac.z));
        float3 w3 = make_float3(1.0/6.0*fraction2.x*fraction.x, 1.0/6.0*fraction2.y*fraction.y, 1.0/6.0*fraction2.z*fraction.z);
        float3 g0 = w0 + w1;
        float3 g1 = w2 + w3;
        // h0 = w1/g0 - 1, shift back by 0.5
        float3 h0 = make_float3((w1/g0).x-0.5+index.x, (w1/g0).y-0.5+index.y, (w1/g0).z-0.5+index.z);
        float3 h1 = make_float3((w3/g1).x+1.5+index.x, (w3/g1).y+1.5+index.y, (w3/g1).z+1.5+index.z);
        // fetch the 8 linear interpolations
        float tex000 = tex3D(data_tex, h0.x, h0.y, h0.z);
        float tex001 = tex3D(data_tex, h0.x, h0.y, h1.z);
        float tex010 = tex3D(data_tex, h0.x, h1.y, h0.z);
        float tex011 = tex3D(data_tex, h0.x, h1.y, h1.z);
        float tex100 = tex3D(data_tex, h1.x, h0.y, h0.z);
        float tex101 = tex3D(data_tex, h1.x, h0.y, h1.z);
        float tex110 = tex3D(data_tex, h1.x, h1.y, h0.z);
        float tex111 = tex3D(data_tex, h1.x, h1.y, h1.z);
        // weigh along the z-direction
        tex000 = lerp(tex001, tex000, g0.z);
        tex010 = lerp(tex011, tex010, g0.z);
        tex100 = lerp(tex101, tex100, g0.z);
        tex110 = lerp(tex111, tex110, g0.z);      
        // weigh along the y-direction
        tex000 = lerp(tex010, tex000, g0.y);
        tex100 = lerp(tex110, tex100, g0.y);
        // weigh along the x-direction
        dest[gid] = lerp(tex100, tex000, g0.x);
      }
    }
  }
"""

class Interp:

  def nearest_neighbor_serial(self,Config,GridData,CameraGrid):

    start_time = time.time()
    
    self.d = griddata((GridData.x,GridData.y,GridData.z), GridData.v, (CameraGrid.x,CameraGrid.y,CameraGrid.z), method='nearest')
    self.d = self.d.reshape([CameraGrid.nx,CameraGrid.ny,CameraGrid.nz])
    self.interp_size = self.d.shape  
    
    print "    Serial interpolation time: ", time.time()-start_time

  def bilinear_parallel(self,Config,TextureData,CameraGrid,interp_texture_kernel,data_tex):
  
    self.interp_texture_kernel = interp_texture_kernel
    self.data_tex = data_tex

    self.shape_sample = [CameraGrid.nx,CameraGrid.ny,CameraGrid.nz]
    self.Nsample = np.int32(self.shape_sample[0] * self.shape_sample[1] * self.shape_sample[2])

    self.data_tex.set_array(TextureData.ary)

    self.d_d = gpu.zeros(self.shape_sample, dtype=np.float32, order="C")

    self.blocksize = (NTPBX, NTPBY, NTPBZ)
    self.gridsize = (int(np.ceil(float(MAXNTHREADS) / np.prod(self.blocksize))), 1)

    self.Nthreads = np.prod(self.blocksize) * np.prod(self.gridsize)
    self.Niterations = np.ceil(float(self.Nsample) / self.Nthreads)

    self.gpukernel_start = cu.Event()
    self.gpukernel_stop = cu.Event()
    self.gpukernel_start.record()

    self.interp_texture_kernel(self.d_d, CameraGrid.z_d, CameraGrid.y_d, CameraGrid.x_d, self.Nsample,  np.int32(self.Niterations), np.int32(self.Nthreads), block=self.blocksize, grid=self.gridsize, texrefs=[self.data_tex])

    self.interp_size = self.shape_sample

    self.gpukernel_stop.record()
    self.gpukernel_stop.synchronize()
    self.kerneltime = self.gpukernel_start.time_till(self.gpukernel_stop)
    print "    Parallel interpolation time: ", self.kerneltime*1e-3
