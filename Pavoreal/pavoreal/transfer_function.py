from include import *

class TransferFunction:

  def construct(self, Config):
    if Config.tfscheme == 'contour':
      self.construct_tf_contour(Config)
    if Config.tfscheme == 'contour_multiwidth':
      self.construct_tf_contour(Config)
    if Config.tfscheme == 'linear':
      self.construct_tf_linear(Config)

  def construct_tf_contour_multiwidth(self, Config):
    self.min = Config.min
    self.max = Config.max

    self.nbins = TF_NBINS
    self.x = np.linspace(self.min,self.max,self.nbins)
    self.y = np.zeros([self.nbins,4])
    
    self.n_contours = Config.n_contours
    self.height = Config.contour_height
    self.location = Config.contour_location
    for i in xrange(self.n_contours):
        for color in xrange(4):
            self.y[:,color] = self.y[:,color] + self.height[i,color] * np.exp(-(self.x- self.location[i])**2/self.contour_width[i])
    self.y[self.y>1] = 1
    # sending the transfer function to the GPU
    self.y_d = cu.to_device(np.float32(self.y))

  def construct_tf_contour(self, Config):
    self.min = Config.min
    self.max = Config.max

    self.nbins = TF_NBINS
    self.x = np.linspace(self.min,self.max,self.nbins)
    self.y = np.zeros([self.nbins,4])
    
    self.n_contours = Config.n_contours
    self.width = (self.max - self.min) / 100.
    self.height = Config.contour_height
    self.location = Config.contour_location
    for i in xrange(self.n_contours):
        for color in xrange(4):
            self.y[:,color] = self.y[:,color] + self.height[i,color] * np.exp(-(self.x- self.location[i])**2/self.width)
    self.y[self.y>1] = 1
    # sending the transfer function to the GPU
    self.y_d = cu.to_device(np.float32(self.y))

  def construct_tf_linear(self, Config):
    self.min = Config.min
    self.max = Config.max

    self.nbins = TF_NBINS
    self.y = np.zeros([self.nbins,4])

    self.cmap = cm.get_cmap(Config.cmap_name, self.nbins) 
    self.cmapvals = self.cmap(np.arange(self.nbins))
    self.y[:, 0:3] = self.cmapvals[:, 0:3]
    self.y[:,3] = np.linspace(Config.alpha_min,Config.alpha_max,self.nbins)
    
    # sending the transfer function to the GPU
    self.y_d = cu.to_device(np.float32(self.y))
