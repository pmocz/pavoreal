from include import *

camera_grid_kernel_source = \
"""
#include<stdio.h>
__global__ void camera_grid_kernel(float* rot, float* x, float* y, float* z, \
                                   int nx, int ny, int nz, int ntot, \
                                   int grid_nx, int grid_ny, int grid_nz, \
                                   float cam2tex_x, float cam2tex_y, float cam2tex_z, \
                                   int image_y, int image_z, \
                                   float shift_x, float shift_y, float shift_z,\
                                   float box_x, float box_y, float box_z,\
                                   int Niterations, int Nthreads )
{
   float r00 = rot[0];
   float r01 = rot[1];
   float r02 = rot[2];
   float r10 = rot[3];
   float r11 = rot[4];
   float r12 = rot[5];
   float r20 = rot[6];
   float r21 = rot[7];
   float r22 = rot[8];
   float xgrid, ygrid, zgrid;

   for (int iteration = 0; iteration < Niterations; ++iteration)
    { 
     int gid = blockIdx.x*blockDim.x + threadIdx.x + iteration * Nthreads;
     if (gid < ntot) {
       xgrid = ((float)(gid/(ny*nz))) * cam2tex_x   - nx*cam2tex_x           + shift_x*(grid_nx - 2) / box_x;
       ygrid = ((float)((gid/nz) % ny)) * cam2tex_y - 0.5*image_y*cam2tex_y  + shift_y*(grid_ny - 2) / box_y;
       zgrid = ((float)(gid%nz)) * cam2tex_z        - 0.5*image_z*cam2tex_z  + shift_z*(grid_nz - 2) / box_z;
       x[gid] = r00*xgrid + r01*ygrid + r02*zgrid + 0.5*(grid_nx - 2);
       y[gid] = r10*xgrid + r11*ygrid + r12*zgrid + 0.5*(grid_ny - 2);
       z[gid] = r20*xgrid + r21*ygrid + r22*zgrid + 0.5*(grid_nz - 2);
     }
    }
}
"""

class CameraGrid:

  def interp_camera_path(self, interval, n_frame, frame_number, camerapath_period):

    if n_frame == 1:
      return interval[0]
    elif (camerapath_period == 0):
      interval_axis = np.linspace(0,1,len(interval))
      interp_axis = np.linspace(0,1,n_frame)
      return np.float32(np.interp(interp_axis, interval_axis, interval))[frame_number]
    else:
      interval_axis = np.linspace(0,1,len(interval))
      interp_axis = np.linspace(0,1,camerapath_period)
      return np.float32(np.interp(interp_axis, interval_axis, interval))[frame_number % camerapath_period]

    
  def camera_view_serial(self, Config, frame_number):

    start_time = time.time()  
  
    self.shift_x = self.interp_camera_path(Config.shift_x,Config.n_frame,frame_number)
    self.shift_y = self.interp_camera_path(Config.shift_y,Config.n_frame,frame_number)
    self.shift_z = self.interp_camera_path(Config.shift_z,Config.n_frame,frame_number)
    self.pitch = self.interp_camera_path(Config.pitch,Config.n_frame,frame_number)
    self.yaw = self.interp_camera_path(Config.yaw,Config.n_frame,frame_number)
    self.roll = self.interp_camera_path(Config.roll,Config.n_frame,frame_number)
  
    self.nx = int(np.ceil(np.max((self.shift_x + np.sqrt(Config.box_x**2+Config.box_y**2+Config.box_z**2) * Config.len2pix,1))))
    self.ny = Config.image_y
    self.nz = Config.image_z  
    self.x, self.y, self.z = np.mgrid[0:self.nx,0:self.ny,0:self.nz]
    self.x = self.x.flatten()
    self.y = self.y.flatten()
    self.z = self.z.flatten()

    # rescale camera grid to texture coordinates
    self.cam2tex_x = ((Config.grid_nx-2)/Config.box_x)/Config.len2pix;
    self.cam2tex_y = ((Config.grid_ny-2)/Config.box_y)/Config.len2pix;
    self.cam2tex_z = ((Config.grid_nz-2)/Config.box_z)/Config.len2pix;
    self.x *= self.cam2tex_x
    self.y *= self.cam2tex_y
    self.z *= self.cam2tex_z
    
    # Center grid at point of rotation, origin is at center of data
    self.x +=            - self.nx*self.cam2tex_x + self.shift_x * (Config.grid_nx-2)/Config.box_x
    self.y += - 0.5*Config.image_y*self.cam2tex_y + self.shift_y * (Config.grid_ny-2)/Config.box_y
    self.z += - 0.5*Config.image_z*self.cam2tex_z + self.shift_z * (Config.grid_nz-2)/Config.box_z
    self.r = np.vstack([self.x, self.y, self.z])

    # Define rotation matrices
    self.rot_x = np.array([[1,0,0],
                           [0, np.cos(self.roll), -np.sin(self.roll)],
                           [0, np.sin(self.roll), np.cos(self.roll)]], dtype=np.float32)
    self.rot_y = np.array([[np.cos(self.pitch), 0, np.sin(self.pitch)],
                           [0,1,0],
                           [-np.sin(self.pitch),0,np.cos(self.pitch)]], dtype=np.float32)
    self.rot_z = np.array([[np.cos(self.yaw), -np.sin(self.yaw), 0],
                           [np.sin(self.yaw), np.cos(self.yaw), 0],
                           [0,0,1]], dtype=np.float32)

    # Rotate coordinates
    self.r = np.dot(self.rot_x, np.dot(self.rot_y, np.dot(self.rot_z, self.r)))
    self.x = self.r[0]
    self.y = self.r[1]
    self.z = self.r[2]

    # Translate coordinates
    self.x += 0.5*(Config.grid_nx-2)
    self.y += 0.5*(Config.grid_ny-2)
    self.z += 0.5*(Config.grid_nz-2)

    print "    Serial camera grid time: ", time.time()-start_time
    
  def camera_grid_parallel(self, Config, frame_number, camera_grid_kernel):

    self.camera_grid_kernel = camera_grid_kernel
    # camera position - interpolate from the given endpoints
    self.shift_x = self.interp_camera_path(Config.shift_x,Config.n_frame,frame_number, Config.camerapath_period)
    self.shift_y = self.interp_camera_path(Config.shift_y,Config.n_frame,frame_number, Config.camerapath_period)
    self.shift_z = self.interp_camera_path(Config.shift_z,Config.n_frame,frame_number, Config.camerapath_period)
    self.pitch = self.interp_camera_path(Config.pitch,Config.n_frame,frame_number, Config.camerapath_period)
    self.yaw = self.interp_camera_path(Config.yaw,Config.n_frame,frame_number, Config.camerapath_period)
    self.roll = self.interp_camera_path(Config.roll,Config.n_frame,frame_number, Config.camerapath_period)

    self.nx = int(np.ceil(np.max((self.shift_x + np.sqrt(Config.box_x**2+Config.box_y**2+Config.box_z**2) * Config.len2pix,1))))
    self.ny = Config.image_y
    self.nz = Config.image_z
    self.ntot = self.nx * self.ny * self.nz

    # rescale camera grid to texture coordinates
    self.cam2tex_x = np.float32(((Config.grid_nx-2)/Config.box_x)/Config.len2pix);
    self.cam2tex_y = np.float32(((Config.grid_ny-2)/Config.box_y)/Config.len2pix);
    self.cam2tex_z = np.float32(((Config.grid_nz-2)/Config.box_z)/Config.len2pix);
    
    # Define rotation matrices
    self.rot_x = np.array([[1,0,0],
                           [0, np.cos(self.roll), -np.sin(self.roll)],
                           [0, np.sin(self.roll), np.cos(self.roll)]], dtype=np.float32)
    self.rot_y = np.array([[np.cos(self.pitch), 0, np.sin(self.pitch)],
                           [0,1,0],
                           [-np.sin(self.pitch),0,np.cos(self.pitch)]], dtype=np.float32)
    self.rot_z = np.array([[np.cos(self.yaw), -np.sin(self.yaw), 0],
                           [np.sin(self.yaw), np.cos(self.yaw), 0],
                           [0,0,1]], dtype=np.float32)
    self.rot = np.dot(self.rot_x, np.dot(self.rot_y, self.rot_z))

    self.grid_start_time = cu.Event()
    self.grid_end_time = cu.Event()
    self.grid_start_time.record()

    self.rot_d = gpu.to_gpu(self.rot)
 
    self.x_d = gpu.empty(int(self.ntot), dtype = np.float32)
    self.y_d = gpu.empty(int(self.ntot), dtype = np.float32)
    self.z_d = gpu.empty(int(self.ntot), dtype = np.float32)
 
    self.blocksize = (NTPB, 1, 1)
    self.gridsize = (int( np.ceil(float(MAXNTHREADS)/NTPB) ), 1, 1)
    self.Nthreads = np.prod(self.blocksize) * np.prod(self.gridsize)
    self.Niterations = np.ceil(float(self.ntot) / self.Nthreads)
 
    camera_grid_kernel(self.rot_d, self.x_d, self.y_d, self.z_d,
                       np.int32(self.nx),  np.int32(self.ny),  np.int32(self.nz),  np.int32(self.ntot),
                       np.int32(Config.grid_nx), np.int32(Config.grid_ny),  np.int32(Config.grid_nz),
                       self.cam2tex_x, self.cam2tex_y, self.cam2tex_z,
                       np.int32(Config.image_y), np.int32(Config.image_z),
                       self.shift_x, self.shift_y, self.shift_z,
                       np.float32(Config.box_x), np.float32(Config.box_y), np.float32(Config.box_z),
                       np.int32(self.Niterations), np.int32(self.Nthreads),
                       block=self.blocksize , grid= self.gridsize)

    self.grid_end_time.record()
    self.grid_end_time.synchronize()
    self.grid_parallel_time = self.grid_start_time.time_till(self.grid_end_time)

    print "    Parallel camera grid time  : ", self.grid_parallel_time * 1e-3   
