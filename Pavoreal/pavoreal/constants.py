# Camera, GridData and raycast
NTPB        = 512
MAXNTHREADS = 2**21

# Interpolation
NTPBX = 4
NTPBY = 4
NTPBZ = 32

# Transfer Function
TF_NBINS = 256
