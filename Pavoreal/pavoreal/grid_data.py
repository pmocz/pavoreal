from include import *

# Define the CUDA nearest neighbour kernel as a string. Brute force approach
# based on http://ieeexplore.ieee.org.ezp-prod1.hul.harvard.edu/stamp/stamp.jsp?tp=&arnumber=5382329&tag=1
nearest_neighbor_kernel_source = \
"""
#include "float.h"

__global__ void nearest_neighbor_kernel(float* d, int pix_x, int pix_y, int pix_z, float* data_x, float* data_y, float* data_z, float* data_v, int N_data, int N_tpb, float min)
{
  // Allocate shared memory in the kernel call
  extern __shared__ float s_data[];
  
  int tid = threadIdx.x;
  int gid = blockIdx.x*blockDim.x + threadIdx.x;
  
  float camera_grid_x = (gid / (pix_y*pix_z)) - pix_x + 0.5;
  float camera_grid_y = (gid / pix_z) % pix_y + 0.5;
  float camera_grid_z = gid % pix_z + 0.5;    
  float min_dist = FLT_MAX;
  float running_val = min;
  float dist;
  int lookup_shift = 0;

  while(lookup_shift < N_data){
    // Copy data to shared memory
    if (lookup_shift+tid < N_data) {
      s_data[tid] = data_x[lookup_shift+tid];
      s_data[tid+N_tpb] = data_y[lookup_shift+tid];
      s_data[tid+2*N_tpb] = data_z[lookup_shift+tid];
      s_data[tid+3*N_tpb] = data_v[lookup_shift+tid];
    }
    else {
      s_data[tid] = data_x[0];   // Pad values if we're the last thread block
      s_data[tid+N_tpb] = data_y[0];
      s_data[tid+2*N_tpb] = data_z[0];
      s_data[tid+3*N_tpb] = data_v[0];
    }
    lookup_shift += N_tpb;
    __syncthreads();
   
    // brute force search
    if (gid < pix_x*pix_y*pix_z){
      for(int i = 0; i < N_tpb; ++i) {
        dist = (camera_grid_x-s_data[i])*(camera_grid_x-s_data[i]) + (camera_grid_y-s_data[i+N_tpb])*(camera_grid_y-s_data[i+N_tpb]) + (camera_grid_z-s_data[i+2*N_tpb])*(camera_grid_z-s_data[i+2*N_tpb]);
        if(dist < min_dist){
          running_val = s_data[i+3*N_tpb];
          min_dist = dist;
        }
      }
    }
    __syncthreads();
  }

  if (gid < pix_x*pix_y*pix_z)
    d[gid] = running_val;
}
"""

bindata_kernel_source = \
"""
#include <stdio.h>
#include <math.h>

__global__ void bindata_kernel(float* x, float* y, float* z, float* data, int Ndata, \
        float* bin_counts, float* bin_values, int nbins_x, int nbins_y, int nbins_z, int Niterations, int Nthreads, int Nsmooth){
  for (int iteration = 0; iteration < Niterations; ++iteration)
   { 
    int gid = blockIdx.x*blockDim.x + threadIdx.x + iteration * Nthreads;
    int binind;
    float weight;
    if (gid < Ndata){
      int xbinind = (int)floor(x[gid] + 0.5);
      int ybinind = (int)floor(y[gid] + 0.5);
      int zbinind = (int)floor(z[gid] + 0.5);
      //int binind = zbinind*(nbins_x*nbins_y) + ybinind*nbins_x + xbinind;
      for( int i = -Nsmooth; i < Nsmooth + 1; i++)
       {
        for( int j = -Nsmooth; j < Nsmooth + 1; j++)
         {
          for( int k = -Nsmooth; k < Nsmooth + 1; k++)
           {
            binind = (xbinind+i)*(nbins_z*nbins_y) + (ybinind+j)*nbins_z + (zbinind+k);
            if (i == 0 && j == 0 && k== 0)
              weight = 1;
            else
              weight = 0.5 / Nsmooth  / sqrt((float) (i * i + j * j + k * k));
            if (binind < nbins_x*nbins_y*nbins_z) {
                    atomicAdd(&bin_counts[binind], weight);
                    atomicAdd(&bin_values[binind], data[gid]*weight);
            }
           }
         }
       }
    }
   }
}
"""

avgbin_kernel2_source = \
"""
__global__ void avgbin_kernel2( float* bin_counts, float* bin_values, float* binned_data, \
                                   int nbins_x, int nbins_y, int nbins_z, int Nbins, float padvalue, int Niterations, int Nthreads)
{
  for (int iteration = 0; iteration < Niterations; ++iteration)
    {
     int gid = blockIdx.x*blockDim.x + threadIdx.x + iteration * Nthreads;
     //int binind = xbinind*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind;
     int xbinind = gid / (nbins_z*nbins_y);
     int ybinind = (gid % (nbins_z*nbins_y))/nbins_z;
     int zbinind = (gid % (nbins_z*nbins_y))%nbins_z;
     float bincount1, bincount2, bincount3, bincount4, bincount5, bincount6;
     float value1, value2, value3, value4, value5, value6;

     if (gid < Nbins) {

       // if no data falls into this bin
       if (bin_counts[gid] == 0) {

         //check if it is not on the edges
         if ((xbinind > 0 && xbinind < nbins_x -1) &&   (ybinind > 0 && ybinind < nbins_y -1) \
            && (zbinind > 0 && zbinind < nbins_z -1))
          {  
            // if not on the edge, read the neighbors counts and values
            bincount1 = bin_counts[ (xbinind-1)*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind ];
            if (bincount1 > 0)
               value1 = bin_values[ (xbinind-1)*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind ];
            else
               value1 = 0;

            bincount2 = bin_counts[ (xbinind+1)*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind ];
            if (bincount2 > 0)
               value2 = bin_values[ (xbinind+1)*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind ];
            else
               value2 = 0;

            bincount3 = bin_counts[ xbinind*(nbins_z*nbins_y) + (ybinind-1)*nbins_z + zbinind ];
            if (bincount3 > 0)
               value3 = bin_values[ xbinind*(nbins_z*nbins_y) + (ybinind-1)*nbins_z + zbinind ];
            else 
               value3 = 0;

            bincount4 = bin_counts[ xbinind*(nbins_z*nbins_y) + (ybinind+1)*nbins_z + zbinind ];
            if (bincount4 > 0)
               value4 = bin_values[ xbinind*(nbins_z*nbins_y) + (ybinind+1)*nbins_z + zbinind ];
            else
               value4 = 0; 

            bincount5 = bin_counts[ xbinind*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind-1 ];
            if (bincount5 > 0)
               value5 = bin_values[ xbinind*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind-1 ];
            else 
               value5 = 0;

            bincount6 = bin_counts[ xbinind*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind+1 ];
            if (bincount6 > 0)
               value6 = bin_values[ xbinind*(nbins_z*nbins_y) + ybinind*nbins_z + zbinind+1 ];
            else
              value6 = 0;

            // if all neighbors are empty, then pad with padvalue
            if (bincount1 + bincount2 + bincount3 + bincount4 + bincount5 + bincount6 == 0)
              binned_data[gid] = padvalue;
            else
              binned_data[gid] = (value1+value2+value3+value4+value5+value6)/(float)(bincount1+bincount2+bincount3+bincount4+bincount5+bincount6);

         } else {
           // if on the edges, just pad with pad value
           binned_data[gid] = padvalue;
         }
       } else {
               binned_data[gid] = bin_values[gid]/((float)bin_counts[gid]);
       }
  
     }
    }
}
"""

class GridData:

  def nearest_neighbor_serial(self, Config, RawData):

    start_time = time.time()
    
    self.nx = Config.grid_nx
    self.ny = Config.grid_ny
    self.nz = Config.grid_nz   
    self.x, self.y, self.z = np.mgrid[0:self.nx,0:self.ny,0:self.nz]
    self.x = self.x.flatten()
    self.y = self.y.flatten()
    self.z = self.z.flatten()
    
    self.v = np.zeros([],dtype=np.float32)
    
    self.data_x = RawData.x * (self.nx - 2) / Config.box_x
    self.data_y = RawData.y * (self.ny - 2) / Config.box_y
    self.data_z = RawData.z * (self.nz - 2) / Config.box_z
    
    self.v = griddata((self.data_x,self.data_y,self.data_z), RawData.v, (self.x-0.5,self.y-0.5,self.z-0.5), method='nearest')

    self.v = self.v.reshape([self.nx,self.ny,self.nz])
    
    # pad border
    self.v[0,:,:] = Config.min
    self.v[:,0,:] = Config.min
    self.v[:,:,0] = Config.min
    self.v[self.nx-1,:,:] = Config.min
    self.v[:,self.ny-1,:] = Config.min
    self.v[:,:,self.nz-1] = Config.min
    
    self.v = self.v.flatten()
    
    print "    Serial grid data time: ", time.time()-start_time

    
  def nearest_neighbor_parallel(self, Data, Config, nearest_neighbor_kernel, ):
    # XXXXXX rewrite to match serial style -- we do not end up using this, use higher order interpolation instead XXX
    self.nearest_neighbor_kernel = nearest_neighbor_kernel
    self.pix_x = np.int32(np.ceil(np.max(-Data.x)))
    self.pix_y = np.int32(Config.image_y)
    self.pix_z = np.int32(Config.image_z)
    self.N_camera_grid = np.int32(self.pix_x*self.pix_y*self.pix_z)
    self.N_data = np.int32(Data.v.shape[0])
    self.x, self.y, self.z = np.mgrid[0:self.pix_x,0:self.pix_y,0:self.pix_z] + 0.5
    self.x = self.x.flatten() - self.pix_x
    self.y = self.y.flatten()
    self.z = self.z.flatten()
    
    self.d_d = gpu.empty([int(self.pix_x),int(self.pix_y),int(self.pix_z)], dtype = np.float32)
    self.data_x_d = gpu.to_gpu(Data.x)
    self.data_y_d = gpu.to_gpu(Data.y)
    self.data_z_d = gpu.to_gpu(Data.z)
    self.data_v_d = gpu.to_gpu(Data.v)
    # Initialize calculation timer
    self.gpu_calc_start = cu.Event()
    self.gpu_calc_end = cu.Event()
    # Start GPU calculation timing
    self.gpu_calc_start.record()
    self.N_tpb = np.int32(NTPB)
    s_size = int(4 * self.N_tpb * Data.v.dtype.itemsize) # Shared memory size (bytes per block)
    self.nearest_neighbor_kernel(self.d_d, self.pix_x, self.pix_y, self.pix_z, self.data_x_d, self.data_y_d, self.data_z_d, self.data_v_d, self.N_data, self.N_tpb, Config.min, block=(int(self.N_tpb),1,1), grid=(int(np.ceil(float(self.N_camera_grid)/self.N_tpb)), 1), shared=s_size)
    self.gpu_calc_end.record()
    self.gpu_calc_end.synchronize()
    self.gpu_calc_time = self.gpu_calc_start.time_till(self.gpu_calc_end)
    print "  GPU calculation time = ", self.gpu_calc_time * 1e-3 
    self.d = self.d_d.get()

    self.d = self.d.reshape([self.pix_x,self.pix_y,self.pix_z])
    self.interp_size = self.d.shape  

  def bin_data0_parallel(self, Config, RawData, bindata_kernel, avgbin_kernel2):

    self.bindata_kernel = bindata_kernel
    self.avgbin_kernel2 = avgbin_kernel2

    self.nx = Config.grid_nx
    self.ny = Config.grid_ny
    self.nz = Config.grid_nz

    self.Ndata = len(RawData.v)
    self.padvalue = Config.min

    # convert to texture units
    self.data_x = RawData.x * (self.nx - 2) / Config.box_x
    self.data_y = RawData.y * (self.ny - 2) / Config.box_y
    self.data_z = RawData.z * (self.nz - 2) / Config.box_z

    self.x_d = gpu.to_gpu(self.data_x)
    self.y_d = gpu.to_gpu(self.data_y)
    self.z_d = gpu.to_gpu(self.data_z)
    self.data_d = gpu.to_gpu(RawData.v)
    self.bin_counts_d = gpu.zeros((self.nx, self.ny, self.nz), dtype = np.float32)
    self.bin_values_d = gpu.zeros((self.nx, self.ny, self.nz), dtype = np.float32)
    self.binned_data_d = gpu.zeros((self.nx, self.ny, self.nz), dtype = np.float32)

    self.blocksize = (NTPB,1,1)
    self.gridsize = (int( np.ceil(float(MAXNTHREADS)/NTPB) ),1,1)

    self.gpu_start = cu.Event()
    self.gpu_stop = cu.Event()
    self.gpu_start.record()

    self.Nthreads = np.prod(self.blocksize) * np.prod(self.gridsize) 
    self.Niterations = np.ceil(float(self.Ndata) / self.Nthreads)
 
    self.bindata_kernel(self.x_d, self.y_d, self.z_d, self.data_d, np.int32(self.Ndata), self.bin_counts_d, self.bin_values_d,
                   np.int32(self.nx), np.int32(self.ny), np.int32(self.nz), np.int32(self.Niterations), np.int32(self.Nthreads),
                   np.int32(Config.n_smooth), block = self.blocksize, grid = self.gridsize)

    self.Nbins = self.nx * self.ny * self.nz
    self.Niterations =  np.ceil(float(self.Nbins) / self.Nthreads)

    self.avgbin_kernel2(self.bin_counts_d, self.bin_values_d, self.binned_data_d,
                       np.int32(self.nx), np.int32(self.ny), np.int32(self.nz), np.int32(self.Nbins), self.padvalue,
                       np.int32(self.Niterations), np.int32(self.Nthreads),
                       block = self.blocksize, grid = self.gridsize)
    self.gpu_stop.record()
    self.gpu_stop.synchronize()
    self.bintime = self.gpu_start.time_till(self.gpu_stop)

    self.v = self.binned_data_d.get()

    print "   Parallel bin data time:", self.bintime*1e-3

  def trivial_grid_data(self, Config, RawData):
   self.nx = Config.grid_nx
   self.ny = Config.grid_ny
   self.nz = Config.grid_nz
   self.v  = np.zeros((self.nx, self.ny, self.nz), dtype=np.float32)
   self.v[1:-1, 1:-1, 1:-1] = RawData.v
   
   # pad border
   self.v[0,:,:] = Config.min
   self.v[:,0,:] = Config.min
   self.v[:,:,0] = Config.min
   self.v[self.nx-1,:,:] = Config.min
   self.v[:,self.ny-1,:] = Config.min
   self.v[:,:,self.nz-1] = Config.min
    
