from include import *

class TextureData:

  def numpy3d_to_array(self,np_array):
    d, h, w = np_array.shape
    descr = cu.ArrayDescriptor3D()
    descr.width = w
    descr.height = h
    descr.depth = d
    descr.format = cu.dtype_to_array_format(np_array.dtype)
    descr.num_channels = 1
    descr.flags = 0
  
    device_array = cu.Array(descr)
  
    copy = cu.Memcpy3D()
    copy.set_src_host(np_array)
    copy.set_dst_array(device_array)
    copy.width_in_bytes = copy.src_pitch = np_array.strides[1]
    copy.src_height = copy.height = h
    copy.depth = d
  
    copy()
  
    return device_array

  def make_texture(self, Config, GridData):

    #self.v = np.reshape(GridData.v, [GridData.nx, GridData.ny, GridData.nz])
    #self.ary = self.numpy3d_to_array(self.v)
    self.ary = self.numpy3d_to_array(GridData.v)
    
