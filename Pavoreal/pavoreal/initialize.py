from include import *

def ConfigSectionMap(Configuration,section):
    dict1 = {}
    options = Configuration.options(section)
    for option in options:
        try:
            dict1[option] = Configuration.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

     
class Config:

  def __init__(self, filename):
    # read config file fields
    self.Configuration = ConfigParser.ConfigParser()
    self.Configuration.read(filename)

    self.datapath         = ConfigSectionMap(self.Configuration,"Data")['datapath']
    self.file_base        = ConfigSectionMap(self.Configuration,"Data")['filebase']
    self.file_ext         = ConfigSectionMap(self.Configuration,"Data")['fileext']
    self.initial_snap     = int(ConfigSectionMap(self.Configuration,"Data")['initialsnapshot'])
    self.final_snap       = int(ConfigSectionMap(self.Configuration,"Data")['finalsnapshot'])
    self.datatype         = ConfigSectionMap(self.Configuration,"Data")['datatype']
    if self.datatype == "PIC":
        self.datafield = ConfigSectionMap(self.Configuration,"Data")['datafield']
        self.postprocess =  ConfigSectionMap(self.Configuration,"Data")['postprocess']
    self.box_x            = np.float32(ConfigSectionMap(self.Configuration,"Data")['boxx'])
    self.box_y            = np.float32(ConfigSectionMap(self.Configuration,"Data")['boxy'])
    self.box_z            = np.float32(ConfigSectionMap(self.Configuration,"Data")['boxz'])
    # further process some of the input
    self.n_snapshot       = self.final_snap - self.initial_snap + 1

    self.outputpath       = ConfigSectionMap(self.Configuration,"Output")['outputpath']
    self.movie_format     = ConfigSectionMap(self.Configuration,"Output")['movieformat']
    self.in_frame_rate    = ConfigSectionMap(self.Configuration,"Output")['movieinfps']
    self.out_frame_rate   = ConfigSectionMap(self.Configuration,"Output")['movieoutfps']
    self.imagewh          = np.float32(ConfigSectionMap(self.Configuration,"Output")['imagewidthheight'].split(','))

    self.grid_nx          = int(ConfigSectionMap(self.Configuration,"Gridding")['gridnx'])
    self.grid_ny          = int(ConfigSectionMap(self.Configuration,"Gridding")['gridny'])
    self.grid_nz          = int(ConfigSectionMap(self.Configuration,"Gridding")['gridnz'])
    self.n_smooth         = int(ConfigSectionMap(self.Configuration,"Gridding")['nsmooth'])

    self.n_frame_per_snap = int(ConfigSectionMap(self.Configuration,"VolumeRendering")['nframepersnap'])
    self.image_y          = int(ConfigSectionMap(self.Configuration,"VolumeRendering")['imagey'])
    self.image_z          = int(ConfigSectionMap(self.Configuration,"VolumeRendering")['imagez'])
    self.shift_x          = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerashiftx'].split(','))
    self.shift_y          = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerashifty'].split(','))
    self.shift_z          = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerashiftz'].split(','))
    self.yaw              = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerayaw'].split(','))    * np.float32(np.pi / 180)
    self.pitch            = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerapitch'] .split(',')) * np.float32(np.pi / 180)
    self.roll             = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['cameraroll'].split(','))   * np.float32(np.pi / 180)
    self.len2pix          = np.float32(ConfigSectionMap(self.Configuration,"VolumeRendering")['cameralengthtopix'])
    self.camerapath_period = int(ConfigSectionMap(self.Configuration,"VolumeRendering")['camerapathperiod'])
    # further process some of the input
    self.n_frame          = self.n_snapshot * self.n_frame_per_snap

    self.min              = np.float32(ConfigSectionMap(self.Configuration,"TransferFunction")['min'])
    self.max              = np.float32(ConfigSectionMap(self.Configuration,"TransferFunction")['max'])
    self.tfscheme         = ConfigSectionMap(self.Configuration,"TransferFunction")['tfscheme']
    if self.tfscheme == 'contour':
        self.contour_v  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourv'].split(',')
        self.contour_r  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourr'].split(',')
        self.contour_g  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourg'].split(',')
        self.contour_b  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourb'].split(',')
        self.contour_a  = ConfigSectionMap(self.Configuration,"TransferFunction")['contoura'].split(',')
    	# further process some of the input
    	self.n_contours = len(self.contour_v)
    	self.contour_location = np.zeros(self.n_contours,dtype=np.float32)
    	self.contour_height = np.zeros([self.n_contours,4],dtype=np.float32)
    	for i in xrange(0,self.n_contours):
      	   self.contour_location[i] = np.float32(self.contour_v[i])
      	   self.contour_height[i,0] = np.float32(self.contour_r[i])
           self.contour_height[i,1] = np.float32(self.contour_g[i])
      	   self.contour_height[i,2] = np.float32(self.contour_b[i])
           self.contour_height[i,3] = np.float32(self.contour_a[i])

    if self.tfscheme == 'linear':
      self.cmap_name   = ConfigSectionMap(self.Configuration,"TransferFunction")['cmapname']
      self.alpha_min   = np.float32(ConfigSectionMap(self.Configuration,"TransferFunction")['alphamin'])
      self.alpha_max   = np.float32(ConfigSectionMap(self.Configuration,"TransferFunction")['alphamax'])

    if self.tfscheme == 'contour_multiwidth':
        self.contour_v  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourv'].split(',')
        self.contour_r  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourr'].split(',')
        self.contour_g  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourg'].split(',')
        self.contour_b  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourb'].split(',')
        self.contour_a  = ConfigSectionMap(self.Configuration,"TransferFunction")['contoura'].split(',')
        self.contour_w  = ConfigSectionMap(self.Configuration,"TransferFunction")['contourw'].split(',')
    	# further process some of the input
    	self.n_contours = len(self.contour_v)
    	self.contour_location = np.zeros(self.n_contours,dtype=np.float32)
    	self.contour_height = np.zeros([self.n_contours,4],dtype=np.float32)
        self.contour_width = np.zeros([self.n_contours,4],dtype=np.float32)
    	for i in xrange(0,self.n_contours):
           self.contour_width[i]    = np.float32(self.contour_w[i])
      	   self.contour_location[i] = np.float32(self.contour_v[i])
      	   self.contour_height[i,0] = np.float32(self.contour_r[i])
           self.contour_height[i,1] = np.float32(self.contour_g[i])
      	   self.contour_height[i,2] = np.float32(self.contour_b[i])
           self.contour_height[i,3] = np.float32(self.contour_a[i])        

    comm = MPI.COMM_WORLD  
    rank = comm.Get_rank()
    if rank == 0:
      print '\n'
      print '####################################'
      print '#                                  #'
      print '#         P A V O R E A L          #'
      print '#                                  #'
      print '####################################' 
      print '\n'
