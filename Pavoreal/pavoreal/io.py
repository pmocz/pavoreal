from include import *

class RawData:

  def read_data(self, Config, snap_id):
    if Config.datatype == 'Arepo':
      print '  reading data ...      ',
      
      file = h5py.File(Config.datapath+Config.file_base+'%03i'%snap_id+Config.file_ext, "r")
      self.v = np.array(file['PartType0/Density'], dtype=np.float32).T
      self.coords = np.array(file['PartType0/Coordinates'], dtype=np.float32).T

      self.x = self.coords[0]
      self.y = self.coords[1]
      self.z = self.coords[2]
      
      self.v = np.log10(self.v)
      print 'min/max=(%f,%f)...      '%(np.min(self.v),np.max(self.v)),
      
      print '[done]'

    if Config.datatype == 'ArepoProcessed':
      print '  reading data ...      ',
      
      file = h5py.File(Config.datapath+Config.file_base+'%03i'%snap_id+Config.file_ext, "r")
      self.v = np.array(file['PartType0/Density'], dtype=np.float32)[0]
      self.coords = np.array(file['PartType0/Coordinates'], dtype=np.float32)

      self.x = self.coords[0]
      self.y = self.coords[1]
      self.z = self.coords[2]
      self.v = np.log10(self.v)
      print 'min/max=(%f,%f)...      '%(np.min(self.v),np.max(self.v)),
      
      print '[done]'      
      
    if Config.datatype == 'GadgetBinary':
      print '  reading data ...      ',

      file = h5py.File(Config.datapath+Config.file_base+'%03i'%snap_id+Config.file_ext, "r")
      self.v = np.array(file['Density'], dtype=np.float32)
      self.x = np.array(file['x'], dtype=np.float32)
      self.y = np.array(file['y'], dtype=np.float32)
      self.z = np.array(file['z'], dtype=np.float32)

      # Normalize positions
      self.xmin , self.xmax = np.min(self.x), np.max(self.x)
      self.ymin , self.ymax = np.min(self.y), np.max(self.y)
      self.zmin , self.zmax = np.min(self.z), np.max(self.z)
      self.x = (self.x - self.xmin) / (self.xmax - self.xmin)
      self.y = (self.y - self.ymin) / (self.ymax - self.ymin)
      self.z = (self.z - self.zmin) / (self.zmax - self.zmin)

      print '[done]'

    if Config.datatype == 'Enzo':
      print '  reading data ...      ',

      file = h5py.File(Config.datapath+Config.file_base+'%03i'%snap_id+Config.file_ext, "r")
      self.v = np.array(file['Density'], dtype=np.float32)
      self.x = np.array(file['x'], dtype=np.float32)
      self.y = np.array(file['y'], dtype=np.float32) 
      self.z = np.array(file['z'], dtype=np.float32)

      # Normalize positions
      self.xmin , self.xmax = np.min(self.x), np.max(self.x)
      self.ymin , self.ymax = np.min(self.y), np.max(self.y)
      self.zmin , self.zmax = np.min(self.z), np.max(self.z)
      self.x = (self.x - self.xmin) / (self.xmax - self.xmin)
      self.y = (self.y - self.ymin) / (self.ymax - self.ymin)
      self.z = (self.z - self.zmin) / (self.zmax - self.zmin)

      self.v = np.log10(self.v)

      print '[done]'


    if Config.datatype == 'PIC':
      print '  reading data ...      ',

      file = h5py.File(Config.datapath+Config.file_base+'%03i'%snap_id+Config.file_ext, "r")
      self.v = np.array(file[Config.datafield], dtype=np.float32)
      self.contrast = 0.25
      self.v = np.float32(eval(Config.postprocess))
      print 'min/max=(%f,%f)...      '%(np.min(self.v),np.max(self.v))
      self.vshape = self.v.shape
      print "PIC data shape: ", self.vshape
      print '[done]'


def save_image(Config, Image, filename):
  fig = plt.figure(frameon=False)
  ax_size = [0,0,1,1]
  fig.add_axes(ax_size)
  plt.imshow(Image.v,origin='lower')
  plt.axis('off')
  fig.set_size_inches(Config.imagewh[0], Config.imagewh[1])
  fig.savefig(Config.outputpath+filename)

  
def save_movie(Config, image_regularexp):
  imagenames = Config.outputpath+image_regularexp
  moviefname = Config.outputpath+Config.file_base.split('_')[0]+Config.movie_format
  command = "ffmpeg -y -r "+Config.in_frame_rate+" -i "+imagenames+" -sameq -r "+Config.out_frame_rate+" "+moviefname
  print "Executing command %s"%command
  os.system(command)  
  print "Saved movie %s"%moviefname




