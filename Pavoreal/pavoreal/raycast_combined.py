from include import *

# Define the CUDA raycast kernel as a string.
raycast_combined_kernel_source = \
"""

__device__ float3 camera_grid(int gid, \
                            float* rot, \
                            int nx, int ny, int nz, \
                            int grid_nx, int grid_ny, int grid_nz, \
                            float cam2tex_x, float cam2tex_y, float cam2tex_z, \
                            float shift_x, float shift_y, float shift_z,\
                            float box_x, float box_y, float box_z )                          
{
   float r00 = rot[0];
   float r01 = rot[1];
   float r02 = rot[2];
   float r10 = rot[3];
   float r11 = rot[4];
   float r12 = rot[5];
   float r20 = rot[6];
   float r21 = rot[7];
   float r22 = rot[8];
   float xgrid, ygrid, zgrid;

   xgrid = ((float)(gid/(ny*nz))) * cam2tex_x   -     nx*cam2tex_x  + shift_x*(grid_nx - 2) / box_x;
   ygrid = ((float)((gid/nz) % ny)) * cam2tex_y - 0.5*ny*cam2tex_y  + shift_y*(grid_ny - 2) / box_y;
   zgrid = ((float)(gid%nz)) * cam2tex_z        - 0.5*nz*cam2tex_z  + shift_z*(grid_nz - 2) / box_z;
   return make_float3(r00*xgrid + r01*ygrid + r02*zgrid + 0.5*(grid_nx - 2),\
                      r10*xgrid + r11*ygrid + r12*zgrid + 0.5*(grid_ny - 2),\
                      r20*xgrid + r21*ygrid + r22*zgrid + 0.5*(grid_nz - 2));
}


inline  __device__ float3 operator+(const float3 &a, const float3 &b) {
  return make_float3(a.x+b.x, a.y+b.y, a.z+b.z);
}

inline  __device__ float3 operator-(const float3 &a, const float3 &b) {
  return make_float3(a.x-b.x, a.y-b.y, a.z-b.z);
}

inline  __device__ float3 operator*(const float3 &a, const float3 &b) {
  return make_float3(a.x*b.x, a.y*b.y, a.z*b.z);
}

inline  __device__ float3 operator/(const float3 &a, const float3 &b) {
  return make_float3(a.x/b.x, a.y/b.y, a.z/b.z);
}

__device__ float lerp(float a, float b, float w) {
  return a + w*(b-a);
}

  texture<float, 3, cudaReadModeElementType> data_tex;
 __device__ float cubic_spline_interp(float3 pos)
 {
      // shift coordinates by -0.5
      float3 coord_grid = make_float3(pos.x-0.5, pos.y-0.5, pos.z-0.5);
      float3 index = make_float3(floor(coord_grid.x), floor(coord_grid.y), floor(coord_grid.z));
      float3 fraction = coord_grid - index;
      float3 one_frac = make_float3(1.0-fraction.x, 1.0-fraction.y, 1.0-fraction.z);
      float3 one_frac2 = one_frac * one_frac;
      float3 fraction2 = fraction * fraction;
      float3 w0 = make_float3(1.0/6.0*one_frac2.x*one_frac.x, 1.0/6.0*one_frac2.y*one_frac.y, 1.0/6.0*one_frac2.z*one_frac.z);
      float3 w1 = make_float3(2.0/3.0-0.5*fraction2.x*(2.0-fraction.x), 2.0/3.0-0.5*fraction2.y*(2.0-fraction.y), 2.0/3.0-0.5*fraction2.z*(2.0-fraction.z));
      float3 w2 = make_float3(2.0/3.0-0.5*one_frac2.x*(2.0-one_frac.x), 2.0/3.0-0.5*one_frac2.y*(2.0-one_frac.y),2.0/3.0-0.5*one_frac2.z*(2.0-one_frac.z));
      float3 w3 = make_float3(1.0/6.0*fraction2.x*fraction.x, 1.0/6.0*fraction2.y*fraction.y, 1.0/6.0*fraction2.z*fraction.z);
      float3 g0 = w0 + w1;
      float3 g1 = w2 + w3;
      // h0 = w1/g0 - 1, shift back by 0.5
      float3 h0 = make_float3((w1/g0).x-0.5+index.x, (w1/g0).y-0.5+index.y, (w1/g0).z-0.5+index.z);
      float3 h1 = make_float3((w3/g1).x+1.5+index.x, (w3/g1).y+1.5+index.y, (w3/g1).z+1.5+index.z);
      // fetch the 8 linear interpolations
      float tex000 = tex3D(data_tex, h0.x, h0.y, h0.z);
      float tex001 = tex3D(data_tex, h0.x, h0.y, h1.z);
      float tex010 = tex3D(data_tex, h0.x, h1.y, h0.z);
      float tex011 = tex3D(data_tex, h0.x, h1.y, h1.z);
      float tex100 = tex3D(data_tex, h1.x, h0.y, h0.z);
      float tex101 = tex3D(data_tex, h1.x, h0.y, h1.z);
      float tex110 = tex3D(data_tex, h1.x, h1.y, h0.z);
      float tex111 = tex3D(data_tex, h1.x, h1.y, h1.z);
      // weigh along the z-direction
      tex000 = lerp(tex001, tex000, g0.z);
      tex010 = lerp(tex011, tex010, g0.z);
      tex100 = lerp(tex101, tex100, g0.z);
      tex110 = lerp(tex111, tex110, g0.z);      
      // weigh along the y-direction
      tex000 = lerp(tex010, tex000, g0.y);
      tex100 = lerp(tex110, tex100, g0.y);
      // weigh along the x-direction
      return lerp(tex100, tex000, g0.x);
}


#include <math.h>
#define CHANNELS 4
texture<float4, 1, cudaReadModeElementType> transferfunc_tex;
__global__ void raycast_combined_kernel(float* rot, \
                  int nx, int ny, int nz, \
                  int npix, \
                  int grid_nx, int grid_ny, int grid_nz, \
                  float cam2tex_x, float cam2tex_y, float cam2tex_z, \
                  float shift_x, float shift_y, float shift_z,\
                  float box_x, float box_y, float box_z,\
                  float min, float max, \
                  float* image_r, float* image_g, float* image_b)
{
    int gid = blockIdx.x*blockDim.x + threadIdx.x;
    int nbins = 256; 
    float r, g, b, a;

    if (gid < npix){
       image_r[gid]=0;
       image_g[gid]=0;
       image_b[gid]=0;
       for (int k = 0; k < nx; k++){	
          int camera_grid_id = gid+k*npix;
          float3 pos = camera_grid(camera_grid_id,rot,nx,ny,nz,grid_nx,grid_ny,grid_nz,cam2tex_x,cam2tex_y,cam2tex_z,shift_x,shift_y,shift_z,box_x,box_y,box_z);
          float interp_val = cubic_spline_interp(pos);
          float bin_q = (nbins-1)*(interp_val-min)/(max-min);
          float4 texval = tex1Dfetch(transferfunc_tex, bin_q);
          r = texval.x;
          g = texval.y;
          b = texval.z;
          a = texval.w;

          image_r[gid] = a*r + (1-a)*image_r[gid];
          image_g[gid] = a*g + (1-a)*image_g[gid];
          image_b[gid] = a*b + (1-a)*image_b[gid];
       }
    }
}
"""

class Image:

  def raycast_parallel(self,Config,frame_number,TextureData,CameraGrid,TransferFunction,raycast_combined_kernel,data_tex,transferfunc_tex):
  
    # kernels and textures
    self.raycast_combined_kernel = raycast_combined_kernel
    self.data_tex = data_tex
    self.transferfunc_tex = transferfunc_tex
    
    # camera position - interpolate from the given endpoints
    self.shift_x = np.float32(CameraGrid.interp_camera_path(Config.shift_x,Config.n_frame,frame_number, Config.camerapath_period))
    self.shift_y = np.float32(CameraGrid.interp_camera_path(Config.shift_y,Config.n_frame,frame_number,Config.camerapath_period))
    self.shift_z = np.float32(CameraGrid.interp_camera_path(Config.shift_z,Config.n_frame,frame_number,Config.camerapath_period))
    self.pitch   = CameraGrid.interp_camera_path(Config.pitch,Config.n_frame,frame_number,Config.camerapath_period)
    self.yaw     = CameraGrid.interp_camera_path(Config.yaw,Config.n_frame,frame_number,Config.camerapath_period)
    self.roll    = CameraGrid.interp_camera_path(Config.roll,Config.n_frame,frame_number,Config.camerapath_period)

    self.nx = int(np.ceil(np.max((self.shift_x + np.sqrt(Config.box_x**2+Config.box_y**2+Config.box_z**2) * Config.len2pix,1)))) # ray length
    self.ny = Config.image_y
    self.nz = Config.image_z
    self.ntot = self.nx * self.ny * self.nz
    self.npix = self.ny * self.nz

    # rescale camera grid to texture coordinates
    self.cam2tex_x = np.float32(((Config.grid_nx-2)/Config.box_x)/Config.len2pix);
    self.cam2tex_y = np.float32(((Config.grid_ny-2)/Config.box_y)/Config.len2pix);
    self.cam2tex_z = np.float32(((Config.grid_nz-2)/Config.box_z)/Config.len2pix);
    
    # Define rotation matrices
    self.rot_x = np.array([[1,0,0],
                           [0, np.cos(self.roll), -np.sin(self.roll)],
                           [0, np.sin(self.roll), np.cos(self.roll)]], dtype=np.float32)
    self.rot_y = np.array([[np.cos(self.pitch), 0, np.sin(self.pitch)],
                           [0,1,0],
                           [-np.sin(self.pitch),0,np.cos(self.pitch)]], dtype=np.float32)
    self.rot_z = np.array([[np.cos(self.yaw), -np.sin(self.yaw), 0],
                           [np.sin(self.yaw), np.cos(self.yaw), 0],
                           [0,0,1]], dtype=np.float32)
    self.rot = np.dot(self.rot_x, np.dot(self.rot_y, self.rot_z))  
    
    # Initialize communication timer
    self.gpu_time_start = cu.Event()
    self.gpu_time_end = cu.Event()

    # Start GPU communication timing
    self.gpu_time_start.record()

    # send/initialize arrays to/on GPU
    self.rot_d = gpu.to_gpu(self.rot)

    # Set transfer function and data texture reference
    self.data_tex.set_array(TextureData.ary)
    self.transferfunc_tex.set_address( TransferFunction.y_d, TransferFunction.y.nbytes)
    self.transferfunc_tex.set_format(cu.array_format.FLOAT, 4)

    # initialize output image
    self.image_r_d = gpu.empty([self.ny, self.nz], dtype = np.float32)
    self.image_g_d = gpu.empty([self.ny, self.nz], dtype = np.float32)
    self.image_b_d = gpu.empty([self.ny, self.nz], dtype = np.float32)
    
    # block and grid size
    self.blocksize = (NTPB, 1, 1)
    self.gridsize = (int( np.ceil(float(self.npix)/NTPB) ), 1, 1)
    
    # main kernel
    raycast_combined_kernel(self.rot_d,
                       np.int32(self.nx), np.int32(self.ny), np.int32(self.nz),  
                       np.int32(self.npix),
                       np.int32(Config.grid_nx), np.int32(Config.grid_ny),  np.int32(Config.grid_nz),
                       self.cam2tex_x, self.cam2tex_y, self.cam2tex_z,
                       self.shift_x, self.shift_y, self.shift_z,
                       np.float32(Config.box_x), np.float32(Config.box_y), np.float32(Config.box_z),
                       Config.min, Config.max, 
                       self.image_r_d, self.image_g_d, self.image_b_d,
                       block=self.blocksize, grid=self.gridsize, 
                       texrefs=[self.data_tex, self.transferfunc_tex])

    # get the image from the GPU
    self.image_r = self.image_r_d.get()
    self.image_g = self.image_g_d.get()
    self.image_b = self.image_b_d.get()

    self.v = np.dstack([self.image_r, self.image_g, self.image_b])
    
    # end timing                   
    self.gpu_time_end.record()
    self.gpu_time_end.synchronize()
    self.gpu_total_time = self.gpu_time_start.time_till(self.gpu_time_end)
    print "    Parallel raycast time: ", self.gpu_total_time*1e-3
  
  
  
  
  
  
