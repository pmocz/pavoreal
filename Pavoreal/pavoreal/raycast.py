from include import *

# Define the CUDA raycast kernel as a string.
raycast_kernel_source = \
"""
#include <math.h>
#define CHANNELS 4
        texture<float4, 1, cudaReadModeElementType> transferfunc_tex;
__global__ void raycast_kernel(float* data, int ray_length, float min, float max, float* image_r, float* image_g, float* image_b, int N_pix)
{
    int gid = blockIdx.x*blockDim.x + threadIdx.x;
    int nbins = 256; 
    float r, g, b, a;

    if (gid < N_pix){
       image_r[gid]=0;
       image_g[gid]=0;
       image_b[gid]=0;
       for (int k = 0; k < ray_length; k++){	  
          float bin_q = (nbins-1)*(data[gid + k * N_pix]-min)/(max-min);
          float4 texval = tex1Dfetch(transferfunc_tex, bin_q);
          r = texval.x;
          g = texval.y;
          b = texval.z;
          a = texval.w;

          image_r[gid] = a*r + (1-a)*image_r[gid];
          image_g[gid] = a*g + (1-a)*image_g[gid];
          image_b[gid] = a*b + (1-a)*image_b[gid];
       }
    }
}
"""

class Image:

  def get_values(self,Config,Interp,TransferFunction):
    self.interp_size = Interp.interp_size
    self.nbins = TransferFunction.nbins
    self.y = TransferFunction.y

  def raycast_serial(self,Config,Interp,TransferFunction):
    # Start Serial Timing
    self.serial_start = time.time()
    self.get_values(Config,Interp,TransferFunction)
    self.data = Interp.d

    self.image_y = Config.image_y
    self.image_z = Config.image_z
    self.v = np.zeros([self.interp_size[1],self.interp_size[2],3])

    for k in xrange(self.interp_size[0]):

        self.bin_idx = np.int32(np.round((self.nbins-1)*(self.data[k,:,:]-Config.min)/(Config.max-Config.min))) # note, rays travel is along x-axis from x=x_max to x=0
        self.bin_idx[self.bin_idx<0] = 0
        self.bin_idx[self.bin_idx>=TransferFunction.nbins] = TransferFunction.nbins-1
        self.r = np.reshape(self.y[self.bin_idx,0],self.bin_idx.shape)
        self.g = np.reshape(self.y[self.bin_idx,1],self.bin_idx.shape)
        self.b = np.reshape(self.y[self.bin_idx,2],self.bin_idx.shape)
        self.a = np.reshape(self.y[self.bin_idx,3],self.bin_idx.shape)

        self.v[:,:,0] = self.a*self.r + (1-self.a)*self.v[:,:,0]
        self.v[:,:,1] = self.a*self.g + (1-self.a)*self.v[:,:,1]
        self.v[:,:,2] = self.a*self.b + (1-self.a)*self.v[:,:,2]

    self.serial_time = time.time() - self.serial_start
    print "    Serial raycast time = ", self.serial_time


  def raycast_parallel(self,Config,Interp,TransferFunction,raycast_kernel, transferfunc_tex):
  
    self.raycast_kernel = raycast_kernel
    self.transferfunc_tex = transferfunc_tex
    self.get_values(Config,Interp,TransferFunction)
    self.ray_length = np.int32(self.interp_size[0])
 
    self.image_y = Config.image_y
    self.image_z = Config.image_z
    self.N_pix = self.image_y * self.image_z

    # Initialize communication timer
    self.gpu_time_start = cu.Event()
    self.gpu_time_end = cu.Event()

    # Start GPU communication timing
    self.gpu_time_start.record()

    # Set transfer function texture reference
    self.transferfunc_tex.set_address( TransferFunction.y_d, TransferFunction.y.nbytes)
    self.transferfunc_tex.set_format(cu.array_format.FLOAT, 4)

    self.image_r_d = gpu.empty([self.image_y, self.image_z], dtype = np.float32)
    self.image_g_d = gpu.empty([self.image_y, self.image_z], dtype = np.float32)
    self.image_b_d = gpu.empty([self.image_y, self.image_z], dtype = np.float32)

    self.N_pix_d = np.int32(self.N_pix)

    self.raycast_kernel(Interp.d_d, self.ray_length, Config.min, Config.max, self.image_r_d, self.image_g_d, self.image_b_d, self.N_pix_d, block=(NTPB,1,1), grid=(int(np.ceil(float(self.N_pix)/NTPB)), 1), texrefs = [self.transferfunc_tex])

    self.image_r = self.image_r_d.get()
    self.image_g = self.image_g_d.get()
    self.image_b = self.image_b_d.get()

    self.v = np.dstack([self.image_r, self.image_g, self.image_b])
    
    self.gpu_time_end.record()
    self.gpu_time_end.synchronize()
    self.gpu_total_time = self.gpu_time_start.time_till(self.gpu_time_end)

    print "    Parallel raycast time      : ", self.gpu_total_time * 1e-3 


