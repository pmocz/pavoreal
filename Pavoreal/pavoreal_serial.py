import pavoreal as pr
from sys import argv
from mpi4py import MPI

def main():

  comm = MPI.COMM_WORLD  
  rank = comm.Get_rank()
  size = comm.Get_size()
  if len(argv) != 2:
    if rank == 0:
      print "Usage: python", argv[0], "[config file]"
    exit()

  MyConfig = pr.initialize.Config(argv[1])
  MyTransferFunction = pr.transfer_function.TransferFunction()
  MyTransferFunction.construct(MyConfig)

  # divide frames among processors
  n = MyConfig.n_snapshot / size
  r = MyConfig.n_snapshot - n*size
  start = n * rank + min(rank,r)
  end   = n * rank + n + min(rank+1,r)
  
  comm.Barrier()
  mpi_start = MPI.Wtime()
  
  for i in xrange(start,end):
    
    MyRawData = pr.io.RawData()
    MyRawData.read_data(MyConfig,MyConfig.initial_snap+i)
   
    MyGridData = pr.grid_data.GridData()
    MyGridData.nearest_neighbor_serial(MyConfig,MyRawData) 

    del MyRawData
    
    for j in xrange(MyConfig.n_frame_per_snap):
      img_id = i*MyConfig.n_frame_per_snap + j
      print '\n calculating frame %i out of %i:\n' % (img_id+1,MyConfig.n_frame)  
    
      MyCameraGrid = pr.camera.CameraGrid()
      MyCameraGrid.camera_view_serial(MyConfig,img_id)
 
      MyInterp = pr.interpolate.Interp()
      MyInterp.nearest_neighbor_serial(MyConfig,MyGridData,MyCameraGrid)

      MyImage = pr.raycast.Image()
      MyImage.raycast_serial(MyConfig,MyInterp,MyTransferFunction)
      #pr.io.save_image(MyConfig,MyImage,"serial_%03i.png" %img_id)

      del MyCameraGrid
      del MyInterp
      del MyImage

  comm.Barrier()
  mpi_stop = MPI.Wtime()  

  if rank == 0:
    print "TOTAL TIME: %f secs" % (mpi_stop - mpi_start)  
  
if __name__ == '__main__':
    
  main()

