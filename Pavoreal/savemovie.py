import os, sys
from sys import argv

if __name__ == '__main__':
  if len(argv) != 5:
    print "Usage: python", argv[0], "[subfolder] [output movie] [in fps] [out fps]"
    print "Example: ", argv[0], "weibel movie_weibel.mp4 12 24"
    exit() 

  subfolder     = argv[1]
  outmoviename  = argv[2]
  in_frame_rate = argv[3]
  out_frame_rate= argv[4]
  outputpath = '../output/'+subfolder+'/'
  prefix = 'parallel_%3d.png'

  imagenames = outputpath+prefix
  moviefname = outputpath+outmoviename

  command = "ffmpeg -y -r "+in_frame_rate+" -i "+imagenames+" -sameq -r "+out_frame_rate+" "+moviefname
  print "Executing command %s"%command
  os.system(command)  
  print "Saved movie %s"%moviefname


