import pavoreal as pr
from sys import argv
from mpi4py import MPI

def main():

  comm = MPI.COMM_WORLD  
  rank = comm.Get_rank()
  size = comm.Get_size()
  if len(argv) != 2:
    if rank == 0:
      print "Usage: python", argv[0], "[config file]"
    exit()

  MyConfig = pr.initialize.Config(argv[1])
  MyTransferFunction = pr.transfer_function.TransferFunction()
  MyTransferFunction.construct(MyConfig)
  # Compile the CUDA kernels
  nearest_neighbor_kernel = pr.cuda_helper.cuda_compile(pr.grid_data.nearest_neighbor_kernel_source,"nearest_neighbor_kernel")
  bindata_kernel = pr.cuda_helper.cuda_compile(pr.grid_data.bindata_kernel_source,"bindata_kernel")
  avgbin_kernel2 = pr.cuda_helper.cuda_compile(pr.grid_data.avgbin_kernel2_source,"avgbin_kernel2") 
  raycast_combined_kernel,data_tex,transferfunc_tex = pr.cuda_helper.cuda_2texture(pr.raycast_combined.raycast_combined_kernel_source,"raycast_combined_kernel", "data_tex", "transferfunc_tex")

  # divide frames among processors
  n = MyConfig.n_snapshot / size
  r = MyConfig.n_snapshot - n*size
  start = n * rank + min(rank,r)
  end   = n * rank + n + min(rank+1,r)
  
  comm.Barrier()
  mpi_start = MPI.Wtime()
  
  for i in xrange(start,end):
    
    MyRawData = pr.io.RawData()
    MyRawData.read_data(MyConfig,MyConfig.initial_snap+i)
   
    MyGridData = pr.grid_data.GridData()
    if MyConfig.datatype == 'PIC':
      MyGridData.trivial_grid_data(MyConfig, MyRawData)
    else:
      MyGridData.bin_data0_parallel(MyConfig,MyRawData,bindata_kernel,avgbin_kernel2)
    
    MyTextureData = pr.texture.TextureData()
    MyTextureData.make_texture(MyConfig,MyGridData)

    del MyRawData
    del MyGridData
    
    for j in xrange(MyConfig.n_frame_per_snap):
      img_id = i*MyConfig.n_frame_per_snap + j
      print '\n calculating frame %i out of %i:\n' % (img_id+1,MyConfig.n_frame)  

      MyCameraGrid = pr.camera.CameraGrid()
      MyImage = pr.raycast_combined.Image()
      MyImage.raycast_parallel(MyConfig,img_id,MyTextureData,MyCameraGrid,MyTransferFunction,raycast_combined_kernel,data_tex,transferfunc_tex)
      pr.io.save_image(MyConfig,MyImage,"parallel_%03i.png" %img_id)

      del MyImage

    del MyTextureData

  comm.Barrier()
  mpi_stop = MPI.Wtime()  

  if rank == 0:
    pr.io.save_movie(MyConfig,"parallel_%03d.png")
    print "TOTAL TIME: %f secs" % (mpi_stop - mpi_start)  
  
if __name__ == '__main__':
    
  main()

